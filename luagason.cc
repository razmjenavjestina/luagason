#include <cstring>
#include <exception>
#include <stdlib.h>

#include "luagason.h"

using namespace std;

static size_t json_obj_size(const JsonValue &j) {
  assert(j.getTag() == JSON_ARRAY || j.getTag() == JSON_OBJECT);
  size_t ret = 0;
  for (const auto &_ __attribute__((unused)): j) ++ret;
  return ret;
}

static void lua_push_json_array(lua_State *L, const JsonValue &j) {
  auto cnt = json_obj_size(j);
  lua_createtable(L, cnt, 0);

  int i = 0;
  for (const auto &p: j) {
    lua_pushinteger(L, ++i);
    lua_push_json_value(L, p->value);
    lua_settable(L, -3);
  }
}

static void lua_push_json_table(lua_State *L, const JsonValue &j) {
  auto cnt = json_obj_size(j);
  lua_createtable(L, 0, cnt);

  for (const auto &p: j) {
    lua_pushstring(L, p->key);
    lua_push_json_value(L, p->value);
    lua_settable(L, -3);
  }
}

void lua_push_json_value(lua_State *L, const JsonValue &j) {
  switch (j.getTag()) {
    case JSON_NUMBER: { lua_pushnumber(L, j.toNumber()); break; }
    case JSON_STRING: { lua_pushstring(L, j.toString()); break; }
    case JSON_TRUE: { lua_pushboolean(L, 1); break; }
    case JSON_FALSE: { lua_pushboolean(L, 0); break; }
    case JSON_NULL: { lua_pushnil(L); break; }
    case JSON_ARRAY: { lua_push_json_array(L, j); break; }
    case JSON_OBJECT: { lua_push_json_table(L, j); break; }
  }
}

void parse_json(lua_State* L, char* ptr) {
  try {
    JsonValue value;
    JsonAllocator alloc;
    char* endptr;
    if (jsonParse(ptr, &endptr, &value, alloc) == JSON_OK)
      lua_push_json_value(L, value);
    else
      lua_pushnil(L);
  } catch (const exception& e) {
    luaL_error(L, e.what());
  }
}

int json_decode(lua_State* L) {
  const char* str = luaL_checkstring(L, 1);
  char* str_cpy = strdup(str);
  parse_json(L, str_cpy);
  free(str_cpy);
  return 1;
}

const struct luaL_Reg methods[] = {
  {"decode", json_decode},
  {nullptr,  nullptr}
};

extern "C" int luaopen_gason(lua_State* L) {
  luaL_register(L, nullptr, methods);
  return 1;
}
